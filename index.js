var cervezas = [
  {
    name: "Estrella Galicia",
    origin: "Galicia",
    clasificacion: "Lager Especial",
    aptoCeliacos: "No",
    apariencia: "De color dorado brillante",
    ingredientes:
      "Agua de la ciudad de A Coruña, maltas de dos carreras (pilsen y tostada), maíz y lúpulos Nugget y Perle Hallertau. Levadura seleccionada Hijos de Rivera",
    src: "./images/bd/especial-25cl.png",
  },
  {
    name: "Estrella Galicia 1906",
    origin: "Galicia",
    clasificacion: "Helles Bock",
    aptoCeliacos: "No",
    apariencia: "De bonito color ámbar brillante.",
    ingredientes:
      "Agua de la ciudad de A Coruña, maltas de dos carreras (pilsen y tostada), maíz y lúpulo Perle Hallertau. Levadura seleccionada Hijos de Rivera",
    src: "./images/bd/reserva-especial.png",
  },
  {
    name: "Estrella Galicia 1906 Red Vintage",
    origin: "Galicia",
    clasificacion: "Doppel Bock",
    aptoCeliacos: "No",
    apariencia: "Intensa cerveza de color oro viejo, brillante",
    ingredientes:
      "Agua de la ciudad de A Coruña, coupage de cuatro maltas con diferentes grados de tueste. Lúpulos Nugget y Sladek. Levadura seleccionada Hijos de Rivera.",
    src: "./images/bd/red-vintage.png",
  },
  {
    name: "Estrella Galicia 1906 Black Coupagebl",
    origin: "Galicia",
    clasificacion: "Doppel Bock",
    aptoCeliacos: "No",
    apariencia: "Intensa cerveza de color oro viejo, brillante",
    ingredientes:
      "Agua de la ciudad de A Coruña, coupage de cuatro maltas con diferentes grados de tueste. Lúpulos Nugget y Sladek. Levadura seleccionada Hijos de Rivera.",
    src: "./images/bd/black-coupage.png",
  },
  {
    name: "La estrella de Galicia",
    origin: "Galicia",
    clasificacion: "Doppel Tipo German Pils.",
    aptoCeliacos: "No",
    apariencia: "Cerveza de color dorado claro., muy transparente.",
    ingredientes:
      "Agua de la ciudad de A Coruña, maltas de dos carreras (pilsen y tostada), lúpulo Sladek. Levadura seleccionada Hijos de Rivera.",
    src: "./images/bd/especial__galicia.png",
  },
];

const view = { viewNow: "main" };

setTimeout(function () {
  alert("Bienvenido a nuestra web.");
}, 2000);

function listener() {
  document.querySelector("#btnHome").addEventListener("click", myBtnHome);
  document
    .querySelector("#btnExclusive")
    .addEventListener("click", myBtnExclusive);
  document.querySelector("#btnAdd").addEventListener("click", myBtnAdd);
  document.querySelector("#btnAbout").addEventListener("click", myBtnAbout);
  document.querySelector("#btnGalery").addEventListener("click", myBtnGalery);
  document.querySelector("#btnsubmit").addEventListener("click", myForm);
}
// window.onload = function () {
//   listener();

// };

myBtnHome = () => {
  if (view.viewNow === "main") {
    alert("Estas en la Home");
  } else {
    view.viewNow = "main";
    document.querySelector("#main").classList.remove("hidden");
    document.querySelector("#exclusive").classList.add("hidden");
    document.querySelector("#add").classList.add("hidden");
    document.querySelector("#detalles").classList.add("hidden");
    document.querySelector("#about").classList.add("hidden");
  }
};
myBtnExclusive = () => {
  if (view.viewNow === "exclusive") {
    alert("Estas en Exclusivas");
  } else {
    view.viewNow = "exclusive";
    document.querySelector("#main").classList.add("hidden");
    document.querySelector("#exclusive").classList.remove("hidden");
    document.querySelector("#add").classList.add("hidden");
    document.querySelector("#detalles").classList.add("hidden");
    document.querySelector("#about").classList.add("hidden");
  }
};
myBtnGalery = () => {
  view.viewNow = "galeria";
  document.querySelector("#main").classList.add("hidden");
  document.querySelector("#exclusive").classList.remove("hidden");
  document.querySelector("#add").classList.add("hidden");
  document.querySelector("#detalles").classList.add("hidden");
  document.querySelector("#about").classList.add("hidden");
};

myBtnAdd = () => {
  if (view.viewNow === "add") {
    alert("Estas en Añadir");
  } else {
    view.viewNow = "add";
    document.querySelector("#main").classList.add("hidden");
    document.querySelector("#exclusive").classList.add("hidden");
    document.querySelector("#add").classList.remove("hidden");
    document.querySelector("#detalles").classList.add("hidden");
    document.querySelector("#about").classList.add("hidden");
  }
};
myBtnAbout = () => {
  if (view.viewNow === "about") {
    alert("Estas en About");
  } else {
    view.viewNow = "about";
    document.querySelector("#main").classList.add("hidden");
    document.querySelector("#exclusive").classList.add("hidden");
    document.querySelector("#add").classList.add("hidden");
    document.querySelector("#detalles").classList.add("hidden");
    document.querySelector("#about").classList.remove("hidden");
  }
};

function showInfo() {
  var element = document.querySelector("#information");
  element.classList.add("active");
}

var namebirra = [];

function addBirra() {
  const exclusivePhoto = document.querySelector(".exclusive__photo");

  cervezas.forEach((cerveza) => {
    const newDiv = document.createElement("div");

    newDiv.classList.add("imgContainer");
    newDiv.innerHTML = `
        <img src="${cerveza.src}">
        <p> ${cerveza.name}</p>
        `;
    exclusivePhoto.appendChild(newDiv);

    var newBtn = document.createElement("button");
    newBtn.type = "button";
    newBtn.innerHTML = `Detalles`;
    newBtn.setAttribute("id", cerveza.name);
    var imgBeerSrc = cerveza.src;
    var imgBeerName = cerveza.name;
    var imgBeerOrigin = cerveza.origin;
    var imgBeerClas = cerveza.clasificacion;
    var imgBeerApar = cerveza.apariencia;
    var imgBeerIngred = cerveza.ingredientes;
    var imgBeerApt = cerveza.aptoCeliacos;

    newBtn.classList.add("btnDetailsBirra");
    newBtn.addEventListener("click", function () {
      namebirra.push(cerveza.name);
      document.querySelector("#exclusive").classList.add("hidden");
      document.querySelector("#detalles").classList.remove("hidden");

      const newDivImage = document.createElement("div");
      newDivImage.classList.add("imgContainerDetails");
      document.querySelector("#detalles").appendChild(newDivImage);
      document.querySelector("#imgDetail").src = imgBeerSrc;

      var myDivDetails = document.querySelector("#BeerDetail");
      myDivDetails.innerHTML = `
            <p>Nombre: ${imgBeerName}</p>
            <p>Origen: ${imgBeerOrigin}</p>
            <p>Clasificación: ${imgBeerClas}</p>
            <p>Apariencia: ${imgBeerApar}</p>
            <p>Ingredientes: ${imgBeerIngred}</p>
            <p>Apto para celiacos: ${imgBeerApt}</p>
            `;
    });
    newDiv.appendChild(newBtn);
  });
}
var people = [
  {
    name: "Andres",
    email: "andres@gmail.com",
    phone: 652000000,
    proposit: "Colaborador",
  },
  {
    name: "Carlos",
    email: "carlos@gmail.com",
    phone: 652000000,
    proposit: "Maestro",
  },
  {
    name: "Antonio",
    email: "antonio@gmail.com",
    phone: 652000000,
    proposit: "Maestro",
  },
  {
    name: "Juan",
    email: "juan@gmail.com",
    phone: 652000000,
    proposit: "Colaborador",
  },
  {
    name: "José",
    email: "jose@gmail.com",
    phone: 652000000,
    proposit: "Colaborador",
  },
  {
    name: "Perico",
    email: "perico@gmail.com",
    phone: 652000000,
    proposit: "Maestro",
  },
];
function peoplelist() {
  var myPeopleList = document.querySelector("#mypeoplelist");
  people.forEach((person) => {
    var peopleDiv = document.createElement("div");
    peopleDiv.classList.add("pForm");
    peopleDiv.setAttribute("id", "peopleDiv");

    peopleDiv.innerHTML = `
    <table class="tableAdd" border="1">
      <td>${person.name}</td>
      <td>${person.email}</td>
      <td>${person.phone}</td>
      <td>${person.proposit}</td>
    </table>
    `;
    myPeopleList.appendChild(peopleDiv);
  });
}

function myForm(ev) {
  ev.preventDefault();
  var myFormName = document.querySelector("#name").value;
  var myFormMail = document.querySelector("#mail").value;
  var myFormPhone = document.querySelector("#phone").value;
  var myFormProposit = document.querySelector("#proposit").value;
  var newPerson = {
    name: myFormName,
    email: myFormMail,
    phone: myFormPhone,
    proposit: myFormProposit,
  };
  var addNew = { ...newPerson };
  console.log(newPerson);
  document.forms[0].reset();
  people.push(addNew);
  console.log(people);
  const removemeAll = document.querySelectorAll("peopleDiv");
  console.log(removemeAll);
  for (const removeme of removemeAll) {
    removeme.remove();
  }
  peoplelist();
}

window.onload = function () {
  listener();
  addBirra();
};
